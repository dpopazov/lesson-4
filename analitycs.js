const fs = require('fs');
const path = require('path');


const gasContent = fs.readFileSync(path.join(__dirname, './audit/gas.csv'), 'utf8');
const waterContent = fs.readFileSync(path.join(__dirname, './audit/water.csv'), 'utf8');

const formatValues = content => {
  const contentArr = content.split('\n');

  const dataByUser = {};

  contentArr.forEach(item => {
    if (!item.length || item.includes('date,value,name')) return;
    else {
      const [date, value, userName] = item.split(',');

      if (!dataByUser[userName]) {
        dataByUser[userName] = [];
      }

      dataByUser[userName].push({ date, value })
    }
  });

  return dataByUser;
}

const groupByUserToString = (data, type) => {

  console.log(`All data for ${type}`);

  Object.keys(data).forEach(key => {
    let resultString = `User ${key} - entered ${data[key].length} times data`

    const fomatedDataToString = data[key].reduce((acc, { date, value }) => {
      return `${acc} \n- ${date} - ${value}`;
    }, '');

    console.log(`${resultString}\n${fomatedDataToString}\n`);
  });
};

groupByUserToString(formatValues(gasContent), 'gas');
groupByUserToString(formatValues(waterContent), 'water');
