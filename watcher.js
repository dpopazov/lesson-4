const fs = require('fs');
const path = require('path');

const distPath = path.join(__dirname, `/dist`);
const isDistExists = fs.existsSync(distPath);

if (!isDistExists) {
  fs.mkdirSync(distPath);
}

fs.watch(path.join(__dirname, './dist'), (eventType, fileName) => {
  const fileThatWasChanged = path.join(__dirname, './dist', fileName);

  if (eventType === 'change') {
    const content = fs.readFileSync(fileThatWasChanged, 'utf8');

    const parsedData = JSON.parse(content);

    switch (parsedData.type) {
      case 'gas':
        fs.appendFile(
          path.join(__dirname, './audit/gas.csv'),
          `\n${parsedData.date},${parsedData.value},${parsedData.name}`,
          () => console.log('Gas file updated')
        );
        break;

      case 'water':
        fs.appendFile(
          path.join(__dirname, './audit/water.csv'),
          `\n${parsedData.date},${parsedData.value},${parsedData.name}`,
          () => console.log('Water file updated')
        );
        break;
    }

    fs.rm(fileThatWasChanged, () => console.log("JSON file removed"));
  }
});
