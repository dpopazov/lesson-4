const readline = require('readline');
const fs = require('fs');
const path = require('path');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const ask = (question, validation) => {
  return new Promise((res, rej) => {
    rl.question(question, data => {
      if (!data.length || (typeof validation === 'function' && !validation(data))) {
        rej(new Error('Invalid input data'));
        rl.close();
      }

      res(data);
    });
  });
};

const askAll = async () => {
  const name = await ask('What is your name? ');
  const type = await ask('What type of data do you want to input? ', data => ['gas', 'water'].includes(data));
  const value = await ask('What is data? ', data => !Number.isNaN(parseInt(data, 10)));

  const date = new Date();

  const data = {
    name,
    type,
    value,
    date: `${date.getMonth()}-${date.getDate()}-${date.getFullYear()}`
  };

  const distPath = path.join(__dirname, `/dist`);
  const isDistExists = fs.existsSync(distPath);

  if (!isDistExists) {
    fs.mkdirSync(distPath);
  }

  fs.writeFile(path.join(distPath, `/data-${Date.now()}.json`), JSON.stringify(data), () => console.log('Data saved'));

  rl.close();
};

askAll();
